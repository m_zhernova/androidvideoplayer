//
// Created by zhernova on 10.03.2020.
//

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavutil/time.h>
}

class Clock
{

public:

   double videoCurrentPts;

   int64_t videoCurrentPtsTime;

   double audioClock;

   double videoClock;

   double getMasterClock( const AVCodecContext *audioContext, int avSyncType, int audioBufferSize );

   double getAudioClock( const AVCodecContext *audioContext, int bufSize );

   double getVideoClock();

   double synchronizeVideo( AVRational timeBase, const AVFrame &srcFrame,
                            double pts )
   {
      if( pts != 0 )
         videoClock = pts;
      else
         pts = videoClock;
      double frameDelay = av_q2d( timeBase );
      frameDelay += srcFrame.repeat_pict * ( frameDelay * 0.5 );
      videoClock += frameDelay;
      return pts;
   }
};
