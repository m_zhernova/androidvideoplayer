//
// Created by zhernova on 22.01.2020.
//

#include "helpers.h"

extern "C"
{
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
#include <libavformat/avformat.h>
}

#include <android/log.h>

const std::string LOG_TAG = "GLProgram";
#define LOGE( fmt, args... ) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG.c_str(), fmt, ##args)

void
setPictureSize( int picWidth, int picHeight, int screenWidth, int screenHeight, int &rezWidth,
                int &rezHeight )
{
   double videoWidthToHeight = (double)picWidth / (double)picHeight;
   rezWidth = picWidth;
   rezHeight = picHeight;
   if( rezWidth > screenWidth )
   {
      rezWidth = screenWidth;
      rezHeight = static_cast<int>((double)rezWidth / videoWidthToHeight);
   }
   if( rezHeight > screenHeight )
   {
      rezHeight = screenHeight;
      rezWidth = static_cast<int>(videoWidthToHeight * rezHeight);
   }
   if( rezWidth < screenWidth && rezHeight < screenHeight )
   {
      if( screenWidth - rezWidth < screenHeight - rezHeight )
      {
         rezWidth = screenWidth;
         rezHeight = static_cast<int>((double)rezWidth / videoWidthToHeight);
      }
      else
      {
         rezHeight = screenHeight;
         rezWidth = static_cast<int>(videoWidthToHeight * rezHeight);
      }
   }
}

void
fillWindowBuffer( const uint8_t *out_buffer, int height, int width, int lineSize, int pixelSize,
                  uint8_t *windowBuffer )
{
   for( int i = 0; i < height; i++ )
   {
      long long bufferStep = ( i * width * pixelSize );
      int bytesToWrite = width * pixelSize;
      int pictureStep = ( i * lineSize );
      if( nullptr != out_buffer )
         memcpy( windowBuffer + bufferStep,
                 out_buffer + pictureStep,
                 static_cast<size_t>(bytesToWrite) );
   }
}

void rescale( const AVCodecContext *videoContext, const AVFrame &frame, int dstWidth, int dstHeight,
              AVPixelFormat format, bool forceCreate, VideoPicture &videoPicture,
              SwsContext **img_convert_context )
{
   if( !img_convert_context )
      return;
   if( nullptr == *img_convert_context || forceCreate )
   {
      sws_freeContext( *img_convert_context );
      *img_convert_context = sws_getContext( videoContext->width, videoContext->height,
                                             videoContext->pix_fmt,
                                             dstWidth, dstHeight,
                                             format,
                                             SWS_FAST_BILINEAR,
                                             nullptr, nullptr, nullptr );
   }
   videoPicture.allocBuffer( static_cast<size_t>(av_image_get_buffer_size( format,
                                                                           dstWidth,
                                                                           dstHeight,
                                                                           1 )) );
   av_image_fill_arrays( videoPicture.picture->data,
                         videoPicture.picture->linesize,
                         videoPicture.buffer,
                         format,
                         dstWidth,
                         dstHeight,
                         1 );
   sws_scale( *img_convert_context,
              (const uint8_t **)frame.data,
              frame.linesize,
              0,
              videoContext->height,
              videoPicture.picture->data,
              videoPicture.picture->linesize );
}

uint8_t **resample( int &dstBufferSize, int srcSamplesNmb, int64_t srcSampleRate,
                    int64_t srcChannelLayout, AVSampleFormat srcSampleFmt,
                    AVSampleFormat dstSampleFmt, int srcChannels, uint8_t **srcData )
{
   SwrContext *swrContext = swr_alloc();
   if( !swrContext )
   {
      swr_free( &swrContext );
      return nullptr;
   }
   av_opt_set_int( swrContext, "in_channel_layout", srcChannelLayout, 0 );
   av_opt_set_int( swrContext, "in_sample_rate", srcSampleRate, 0 );
   av_opt_set_sample_fmt( swrContext, "in_sample_fmt", srcSampleFmt, 0 );
   av_opt_set_int( swrContext, "out_channel_layout", srcChannelLayout, 0 );
   av_opt_set_int( swrContext, "out_sample_rate", srcSampleRate, 0 );
   av_opt_set_sample_fmt( swrContext, "out_sample_fmt", dstSampleFmt, 0 );
   int ret = swr_init( swrContext );
   if( ret < 0 )
   {
      swr_free( &swrContext );
      return nullptr;
   }
   int dstLinesize;
   uint8_t **dstData = nullptr;
   int64_t dst_nb_samples =
         av_rescale_rnd( srcSamplesNmb, srcSampleRate, srcSampleRate, AV_ROUND_UP );
   av_samples_alloc_array_and_samples( &dstData,
                                       &dstLinesize,
                                       srcChannels,
                                       static_cast<int>(dst_nb_samples),
                                       dstSampleFmt,
                                       1 );
   dst_nb_samples = av_rescale_rnd( swr_get_delay( swrContext, srcSampleRate ) + srcSamplesNmb,
                                    srcSampleRate,
                                    srcSampleRate,
                                    AV_ROUND_UP );
   ret = swr_convert( swrContext,
                      dstData,
                      static_cast<int>(dst_nb_samples),
                      (const uint8_t **)srcData,
                      srcSamplesNmb );
   if( ret < 0 )
   {
      swr_free( &swrContext );
      return nullptr;
   }
   dstBufferSize = av_samples_get_buffer_size( &dstLinesize, srcChannels, ret, dstSampleFmt, 0 );
   swr_free( &swrContext );
   return dstData;
}

jint getChannelsMask( jclass clazz, int channelsCount, JNIEnv *env )
{
   jfieldID id = nullptr;
   jfieldID idSecond = nullptr;
   switch( channelsCount )
   {
      case 1:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_MONO", "I" );
         break;
      case 2:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_STEREO", "I" );
         break;
      case 3:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_STEREO", "I" );
         idSecond = env->GetStaticFieldID( clazz, "CHANNEL_OUT_FRONT_CENTER", "I" );
         break;
      case 4:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_QUAD", "I" );
         break;
      case 5:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_QUAD", "I" );
         idSecond = env->GetStaticFieldID( clazz, "CHANNEL_OUT_FRONT_CENTER", "I" );
         break;
      case 6:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_5POINT1", "I" );
         break;
      case 7:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_5POINT1", "I" );
         idSecond = env->GetStaticFieldID( clazz, "CHANNEL_OUT_BACK_CENTER", "I" );
         break;
      case 8:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_7POINT1", "I" );
         break;
      default:
         id = env->GetStaticFieldID( clazz, "CHANNEL_OUT_DEFAULT", "I" );
         break;
   }
   if( !idSecond )
      return env->GetStaticIntField( clazz, id );
   return env->GetStaticIntField( clazz, id ) | env->GetStaticIntField( clazz, idSecond );
}

GLuint createTexture( int width, int height )
{
   GLuint mTextureHandle;
   glGenTextures( 1, &mTextureHandle );
   GLuint textureID = mTextureHandle;
   glBindTexture( GL_TEXTURE_2D, textureID );
   glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
   glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
   glBindTexture( GL_TEXTURE_2D, 0 );
   return textureID;
}

GLuint updateTexture( GLuint tex, FrameOut *frameOut )
{
   int w;
   int h;
   GLuint currentTexture = tex;
   int miplevel = 0;
   glGetTexLevelParameteriv( GL_TEXTURE_2D, miplevel, GL_TEXTURE_WIDTH, &w );
   glGetTexLevelParameteriv( GL_TEXTURE_2D, miplevel, GL_TEXTURE_HEIGHT, &h );
   if( w != frameOut->params.first || h != frameOut->params.second || tex == -1 )
   {
      if( tex != -1 )
         glDeleteTextures( 1, &currentTexture );
      currentTexture = createTexture( frameOut->params.first, frameOut->params.second );
   }
   glBindTexture( GL_TEXTURE_2D, static_cast<GLuint>(currentTexture) );
   glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, frameOut->params.first,
                    frameOut->params.second, GL_RGBA, GL_UNSIGNED_BYTE,
                    frameOut->frame.get() );
   glBindTexture( GL_TEXTURE_2D, 0 );
   return currentTexture;
}


int draw( GLuint texture, GLuint sampler2d, GLuint vPosition, GLuint uMVPMatrix,
          GLuint vTexturePosition )
{
   float mvpMatrix[] = {
         1, 0, 0, 0,
         0, 1, 0, 0,
         0, 0, 1, 0,
         0, 0, 0, 1,
   };
   static float POSITION_MATRIX[] = {
         -1, -1, 0,  // X1,Y1,Z1
         1, -1, 0,  // X2,Y2,Z2
         -1, 1, 0,  // X3,Y3,Z3
         1, 1, 0,  // X4,Y4,Z4
   };
   static float TEXTURE_COORDS[] = {
         0, 1, // X1,Y1
         1, 1, // X2,Y2
         0, 0, // X3,Y3
         1, 0, // X4,Y4
   };
   glBindTexture( GL_TEXTURE_2D, texture );
   glUniform1i( sampler2d, 0 );
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
   glUniformMatrix4fv( uMVPMatrix, 1, GL_FALSE, mvpMatrix );
   glVertexAttribPointer( vPosition,
                          3,
                          GL_FLOAT,
                          static_cast<GLboolean>(false),
                          0,
                          POSITION_MATRIX );
   glEnableVertexAttribArray( vPosition );
   glVertexAttribPointer( vTexturePosition,
                          2,
                          GL_FLOAT,
                          static_cast<GLboolean>(false),
                          0,
                          TEXTURE_COORDS );
   glEnableVertexAttribArray( vTexturePosition );
   glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
   glDisableVertexAttribArray( vPosition );
   glDisableVertexAttribArray( vTexturePosition );
   return 0;
}

GLuint loadShader( const char *strSource, GLenum shaderType )
{
   int compiled;
   GLuint shader = glCreateShader( shaderType );
   glShaderSource( shader, 1, &strSource, nullptr );
   glCompileShader( shader );
   glReleaseShaderCompiler();
   glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
   if( compiled != GL_TRUE )
   {
      LOGE( "failed to compile shader" );
      int length = NR_OPEN;
      glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &length );
      char log[length];
      glGetShaderInfoLog( shader, length, &length, log );
      LOGE( "%s", log );
      return static_cast<GLuint>(-1);
   }
   return shader;
}

GLuint createProgram()
{
   static const std::string VERTEX_SHADER =
         "attribute vec4 vPosition; \n"
         "varying vec2 v_texCoord; \n"
         "uniform mat4 uMVPMatrix;"
         "attribute vec2 vTextureCoordinate; \n"
         "void main() \n"
         "{ \n"
         " v_texCoord = vTextureCoordinate; \n"
         " gl_Position = vPosition; \n"
         "}";
   static const std::string FRAGMENT_SHADER =
         "precision mediump float; \n"
         "varying vec2 v_texCoord; \n"
         "uniform sampler2D uTexture; \n"
         "void main() \n"
         "{ \n"
         " gl_FragColor = texture2D(uTexture, v_texCoord); \n"
         "}";
   GLint link = GL_FALSE;
   GLuint iVShader = loadShader( VERTEX_SHADER.c_str(), GL_VERTEX_SHADER );
   GLuint iFShader = loadShader( FRAGMENT_SHADER.c_str(), GL_FRAGMENT_SHADER );
   GLuint iProgId = glCreateProgram();
   glAttachShader( iProgId, iVShader );
   glAttachShader( iProgId, iFShader );
   glLinkProgram( iProgId );
   glGetProgramiv( iProgId, GL_LINK_STATUS, &link );
   if( link != GL_TRUE )
   {
      LOGE( "failed to link program" );
      int length;
      glGetProgramiv( iProgId, GL_INFO_LOG_LENGTH, &length );
      char log[length];
      glGetProgramInfoLog( iProgId, length, &length, log );
      LOGE( "%s", log );
      return static_cast<GLuint>(-1);
   }
   return iProgId;
}
