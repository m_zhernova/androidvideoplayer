//
// Created by zhernova on 10.03.2020.
//

#include "clock.h"
#include "helpers.h"

double Clock::getMasterClock( const AVCodecContext *audioContext, int avSyncType,
                              int audioBufferSize )
{
   switch( avSyncType )
   {
      case e_AV_SYNC_VIDEO_MASTER:
         return getVideoClock();
      case e_AV_SYNC_AUDIO_MASTER:
         return getAudioClock( audioContext, audioBufferSize );
      case e_AV_SYNC_EXTERNAL_MASTER:
         return av_gettime() / 1000000.0;
      default:
         return 0;
   }
}

double Clock::getAudioClock( const AVCodecContext *audioContext, int bufSize )
{
   double pts = audioClock;
   int bytesPerSec = 0;
   bytesPerSec = audioContext->sample_rate * audioContext->channels * 2;
   if( bytesPerSec )
      pts -= (double)bufSize / bytesPerSec;
   return pts;
}

double Clock::getVideoClock()
{
   double delta = ( av_gettime() - videoCurrentPtsTime ) / 1000000.0;
   return videoCurrentPts + delta;
}
