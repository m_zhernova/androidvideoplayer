//
// Created by zhernova on 10.03.2020.
//

#pragma once

#include <mutex>

extern "C"
{
#include <libavformat/avformat.h>
}

class Seeker
{
private:
   std::mutex m_mutex;
   bool m_seekReq;
   int m_flags;
   int64_t m_position;

public:
   int64_t streamSeek( int percentToSeek, int64_t duration );

   bool seekFile( AVFormatContext *pFormatCtx );
};