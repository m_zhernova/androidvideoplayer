//
// Created by zhernova on 11.03.2020.
//

#include "stream_info.h"

void StreamInfo::init( int streamIndex, AVStream *avStream, AVCodecContext *context )
{
   streamInd = streamIndex;
   stream = avStream;
   codecContext = context;
}

int StreamInfo::decode( AVFrame *frame, bool *gotFrame, AVPacket *packet )
{
   int error;
   *gotFrame = false;
   if( nullptr != packet )
   {
      error = avcodec_send_packet( codecContext, packet );
      if( error < 0 )
         return error == AVERROR_EOF ? 0 : error;
   }
   error = avcodec_receive_frame( codecContext, frame );
   if( error < 0 && error != AVERROR_EOF )
      return error;
   if( error >= 0 )
      *gotFrame = true;
   return 0;
}

AVSampleFormat StreamInfo::getFormat()
{
   return codecContext->sample_fmt;
}

AVRational StreamInfo::getTimeBase()
{
   return codecContext->time_base;
}

void StreamInfo::flushBuffers()
{
   avcodec_flush_buffers( codecContext );
}

double StreamInfo::getFrameRate()
{
   return static_cast<double>(stream->avg_frame_rate.num) / stream->avg_frame_rate.den;
}

StreamInfo::~StreamInfo()
{
   avcodec_free_context( &codecContext );
}
