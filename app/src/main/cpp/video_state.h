//
// Created by zhernova on 11.03.2020.
//

#pragma once

extern "C"
{
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/pixdesc.h>
}

#include <mutex>
#include "helpers.h"
#include "stream_info.h"
#include <android/log.h>

#define VIDEO_PICTURE_QUEUE_SIZE       1

struct PictQueue
{
   VideoPicture queue[VIDEO_PICTURE_QUEUE_SIZE];
   int size, readIndex, writeIndex;
   std::mutex mutex;
   std::condition_variable condition;
};

class VideoState
{
   PictQueue m_pictQueue;
   SwsContext *m_swsContext;
   bool m_forceCreate = false;
public:

   FrameOut m_frameOut;

   StreamInfo m_videoStream;

   ~VideoState();

   int queuePicture( double pts, AVFrame *frame, AVCodecContext *videoContext, int *state );

   int openVideoComponent( unsigned int streamIndex, AVCodecContext *codecCtx,
                           AVCodec *codec, AVStream *stream );

   AVCodecContext *getContext();

   void fillFrame();

   int getPictQueueSize();

   double getCurrentPts();

   void notify();

};
