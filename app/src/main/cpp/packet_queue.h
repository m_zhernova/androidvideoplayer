//
// Created by zhernova on 10.03.2020.
//

#pragma  once

#include <mutex>
#include "helpers.h"

extern "C"
{
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

class PacketQueue
{

private:

   AVPacketList *m_first;

   AVPacketList *m_last;

   int m_packetsNum;

   int m_size;

   std::mutex m_mutex;

   std::condition_variable m_condition;

public:

   int put( const AVPacket &packet, const AVPacket &flushPacket );

   int get( int block, AVPacket &packet, int *state );

   void flush();

   void notify();

   int getSize();


};

