//
// Created by zhernova on 11.03.2020.
//

#pragma once

extern "C"
{
#include <libavformat/avformat.h>
};

struct StreamInfo
{
   int streamInd;
   AVStream *stream;
   AVCodecContext *codecContext;

   ~StreamInfo();

   void init( int streamIndex, AVStream *avStream, AVCodecContext *context );

   int decode( AVFrame *frame, bool *gotFrame, AVPacket *packet );

   AVSampleFormat getFormat();

   AVRational getTimeBase();

   void flushBuffers();

   double getFrameRate();
};
