//
// Created by zhernova on 10.03.2020.
//

#include "packet_queue.h"
#include "video_lib.h"


int PacketQueue::put( const AVPacket &packet, const AVPacket &flushPacket )
{
   std::unique_lock<std::mutex> locker( m_mutex );
   auto *packetList = static_cast<AVPacketList *>(av_malloc( sizeof( AVPacketList ) ));
   if( nullptr != packetList )
   {
      packetList->pkt = packet;
      packetList->next = nullptr;
      if( nullptr == m_last )
         m_first = packetList;
      else
         m_last->next = packetList;
      m_last = packetList;
      m_packetsNum++;
      m_size += packetList->pkt.size + sizeof( *packetList );
      m_condition.notify_all();
   }
   else if( packet.data != flushPacket.data )
      return -1;
   return 0;
}

int PacketQueue::get( int block, AVPacket &packet, int *state )
{
   AVPacketList *packetList = nullptr;
   int ret = 0;
   std::unique_lock<std::mutex> locker( m_mutex );
   while( true )
   {
      if( *state == e_QUIT || *state == e_OPEN_OTHER )
      {
         ret = -1;
         break;
      }
      packetList = m_first;
      if( nullptr != packetList )
      {
         m_first = packetList->next;
         if( nullptr == m_first )
            m_last = nullptr;
         m_packetsNum--;
         m_size -= packetList->pkt.size + sizeof( *packetList );
         packet = packetList->pkt;
         av_free( packetList );
         ret = 1;
         break;
      }
      else if( !block )
      {
         ret = 0;
         break;
      }
      else
      {
         m_condition.wait( locker );
      }
   }
   return ret;
}

void PacketQueue::flush()
{
   std::unique_lock<std::mutex> locker( m_mutex );
   AVPacketList *packetList;
   AVPacketList *tempPacketList;
   for( packetList = m_first; nullptr != packetList; packetList = tempPacketList )
   {
      tempPacketList = packetList->next;
      av_packet_unref( &packetList->pkt );
      av_free( packetList );
   }
   m_last = nullptr;
   m_first = nullptr;
   m_packetsNum = 0;
   m_size = 0;
}

void PacketQueue::notify()
{
   m_condition.notify_all();
}

int PacketQueue::getSize()
{
   return m_size;
}
