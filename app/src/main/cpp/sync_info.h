//
// Created by zhernova on 11.03.2020.
//

#pragma once

#include "helpers.h"

#define DEFAULT_AV_SYNC_TYPE           e_AV_SYNC_AUDIO_MASTER

struct SyncInfo
{
   int avSyncType;
   double audioDiffCum;
   double audioDiffAvgCoef;
   double audioDiffThreshold;
   int audioDiffAvgCount;

   SyncInfo()
   {
      avSyncType = DEFAULT_AV_SYNC_TYPE;
   }
};
