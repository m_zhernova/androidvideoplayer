//
// Created by zhernova on 22.01.2020.
//
#include <jni.h>
#include "video_lib.h"
#include "helpers.h"
#include <GLES3/gl31.h>
#include <EGL/egl.h>
#include <android/log.h>

const std::string MVP_MATRIX = "uMVPMatrix";
const std::string POSITION = "vPosition";
const std::string TEXTURE_COORDINATE = "vTextureCoordinate";

GLuint vPosition;
GLuint vTexturePosition;
GLint sampler2d;
int uMVPMatrix;
GLuint tex;

extern "C" JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_init( JNIEnv *env, jclass /*clazz*/, jobject seekBar,
                                                  jdouble width, jdouble height )
{
   VideoPlayer::instance().setParams( seekBar, width, height, env );
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_seekPercent( JNIEnv */*env*/, jobject /*obj*/,
                                                         jint percent_to_seek )
{
   VideoPlayer::instance().seekPercent( percent_to_seek );
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_pauseOrContinue( JNIEnv */*env*/, jobject /*obj*/ )
{
   VideoPlayer::instance().pauseOrContinue();
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_openFile( JNIEnv *env, jclass /*clazz*/,
                                                      jstring file_name )
{
   jboolean isCopy;
   const char *convertedValue = env->GetStringUTFChars( file_name, &isCopy );
   std::string string = std::string( convertedValue );
   VideoPlayer::instance().openFile( string );
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_MainActivity_close( JNIEnv */*env*/, jclass /*clazz*/ )
{
   VideoPlayer::instance().joinAllThreads( 1 );
}

extern "C"
void JNICALL
Java_com_example_myapplication_OpenGLRenderer_onChanged( JNIEnv */*env*/, jobject, jint width,
                                                         jint height )
{
   std::string buffer;
   buffer = "---onChanged called------\n";
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer.c_str() );
   glViewport( 0, 0, width, height );
   if( VideoPlayer::instance().getScreenParams().first != width ||
       VideoPlayer::instance().getScreenParams().second != height )
      VideoPlayer::instance().setParams( nullptr, width, height, nullptr );
}

extern "C"
void JNICALL
Java_com_example_myapplication_OpenGLRenderer_onDraw( JNIEnv */*env*/, jobject )
{

   int rezWidth;
   int rezHeight;
   setPictureSize( VideoPlayer::instance().getFrameOut()->params.first,
                   VideoPlayer::instance().getFrameOut()->params.second,
                   VideoPlayer::instance().getScreenParams().first,
                   VideoPlayer::instance().getScreenParams().second, rezWidth,
                   rezHeight );
   int x = 0;
   int y = 0;
   if( VideoPlayer::instance().getScreenParams().first > rezWidth )
      x += ( VideoPlayer::instance().getScreenParams().first - rezWidth ) / 2;
   if( VideoPlayer::instance().getScreenParams().second > rezHeight )
      y += ( VideoPlayer::instance().getScreenParams().second - rezHeight ) / 2;
   glViewport( x, y, rezWidth, rezHeight );
   if( VideoPlayer::instance().getFlag() )
   {
      std::unique_lock<std::mutex> lock( VideoPlayer::instance().getFrameOut()->mutex );
      char buffer[100];
      sprintf( buffer, "start waiting for isWritten\n" );
      __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
      VideoPlayer::instance().getFrameOut()->isWritten.wait( lock );
      sprintf( buffer, "end waiting for isWritten\n" );
      __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
      tex = updateTexture( tex, VideoPlayer::instance().getFrameOut() );
      draw( tex,
            static_cast<GLuint>(sampler2d),
            vPosition,
            static_cast<GLuint>(uMVPMatrix),
            vTexturePosition );
      VideoPlayer::instance().getFrameOut()->mutex.unlock();
   }
   else
      draw( tex,
            static_cast<GLuint>(sampler2d),
            vPosition,
            static_cast<GLuint>(uMVPMatrix),
            vTexturePosition );
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_myapplication_OpenGLRenderer_onCreated( JNIEnv */*env*/, jobject /*obj*/ )
{
   GLuint programId = createProgram();
   glUseProgram( programId );
   vPosition = static_cast<GLuint>(glGetAttribLocation( programId, POSITION.c_str() ));
   vTexturePosition =
         static_cast<GLuint>(glGetAttribLocation( programId, TEXTURE_COORDINATE.c_str() ));
   uMVPMatrix = glGetUniformLocation( programId, MVP_MATRIX.c_str() );
   sampler2d = glGetUniformLocation( programId, "uTexture" );
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_myapplication_MainActivity_getPercent( JNIEnv *env, jobject thiz )
{
   return VideoPlayer::instance().getPercent();
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_myapplication_MainActivity_isPlaying( JNIEnv *env, jobject thiz )
{
   return static_cast<jboolean>(VideoPlayer::instance().getState() == e_PLAYING);
}
extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_myapplication_MainActivity_isPaused( JNIEnv *env, jobject thiz )
{
   return static_cast<jboolean>(VideoPlayer::instance().getState() == e_PAUSED);
}