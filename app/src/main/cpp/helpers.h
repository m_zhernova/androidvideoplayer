//
// Created by zhernova on 22.01.2020.
//

#pragma once

#include <jni.h>
#include <GLES3/gl31.h>
#include <mutex>
#include "frame_out.h"
#include "video_picture.h"

struct AVCodecContext;
struct SwsContext;
struct AVFrame;

enum
{
   e_AV_SYNC_AUDIO_MASTER,
   e_AV_SYNC_VIDEO_MASTER,
   e_AV_SYNC_EXTERNAL_MASTER,
};

enum
{
   e_UNINITILIZED = 0,
   e_PLAYING = 1,
   e_PAUSED = 2,
   e_OPEN_OTHER = 3,
   e_QUIT = 4,
};

void setPictureSize( int picWidth, int picHeight, int screenWidth, int screenHeight, int &rezWidth,
                     int &rezHeight );

void fillWindowBuffer( const uint8_t *out_buffer, int height, int width, int lineSize,
                       int pixelSize, uint8_t *windowBuffer );

void rescale( const AVCodecContext *videoContext, const AVFrame &frame, int dstWidth, int dstHeight,
              AVPixelFormat format, bool forceCreate, VideoPicture &videoPicture,
              SwsContext **img_convert_context );

uint8_t **resample( int &dstBufferSize, int srcSamplesNmb, int64_t srcSampleRate,
                    int64_t srcChannelLayout, AVSampleFormat srcSampleFmt,
                    AVSampleFormat dstSampleFmt, int srcChannels, uint8_t **srcData );

jint getChannelsMask( jclass clazz, int channelsCount, JNIEnv *env );

GLuint createProgram();

GLuint loadShader( const char *strSource, GLenum shaderType );

int draw( GLuint texture, GLuint sampler2d, GLuint vPosition, GLuint uMVPMatrix,
          GLuint vTexturePosition );

GLuint updateTexture( GLuint tex, FrameOut *bytes );

GLuint createTexture( int width, int height );
