//
// Created by zhernova on 11.03.2020.
//

#include <libavutil/time.h>
#include "video_state.h"

int VideoState::queuePicture( double pts, AVFrame *frame, AVCodecContext *videoContext,
                              int *state )
{
   {
      std::unique_lock<std::mutex> lock( m_pictQueue.mutex );
      while( m_pictQueue.size >= VIDEO_PICTURE_QUEUE_SIZE &&
             ( *state == e_PLAYING || *state == e_PAUSED ) )
      {
         __android_log_write( ANDROID_LOG_ERROR, "Warning", "queuePicture waits" );
         m_pictQueue.condition.wait( lock );
         __android_log_write( ANDROID_LOG_ERROR, "Warning", "queuePicture end waiting" );
      }
      m_pictQueue.queue[m_pictQueue.writeIndex].pts = pts;
      rescale( videoContext,
               *frame,
               m_frameOut.params.first,
               m_frameOut.params.second,
               AV_PIX_FMT_RGBA,
               m_forceCreate,
               m_pictQueue.queue[m_pictQueue.writeIndex],
               &m_swsContext );
      if( m_forceCreate )
         m_forceCreate = false;
      m_pictQueue.queue[m_pictQueue.writeIndex].width = m_frameOut.params.first;
      m_pictQueue.queue[m_pictQueue.writeIndex].height = m_frameOut.params.second;
      if( ++m_pictQueue.writeIndex == VIDEO_PICTURE_QUEUE_SIZE )
         m_pictQueue.writeIndex = 0;
      m_pictQueue.size++;
   }
   return 0;
}

void VideoState::fillFrame()
{
   std::unique_lock<std::mutex> frameLocker( m_frameOut.mutex );
   std::unique_lock<std::mutex> queueLocker( m_pictQueue.mutex );
   if( ++m_pictQueue.readIndex == VIDEO_PICTURE_QUEUE_SIZE )
      m_pictQueue.readIndex = 0;
   m_pictQueue.size--;
   if( m_pictQueue.queue[m_pictQueue.readIndex].picture->data[0] )
      fillWindowBuffer( std::ref( m_pictQueue.queue[m_pictQueue.readIndex].picture->data[0] ),
                        m_frameOut.params.second,
                        m_frameOut.params.first,
                        *m_pictQueue.queue[m_pictQueue.readIndex].picture->linesize,
                        av_get_bits_per_pixel( av_pix_fmt_desc_get( AV_PIX_FMT_RGBA ) ) / 8,
                        m_frameOut.frame.get() );
   m_pictQueue.queue[m_pictQueue.readIndex].freeBuffer();
   m_pictQueue.condition.notify_all();
}

void VideoState::notify()
{
   m_pictQueue.condition.notify_all();
}


int VideoState::openVideoComponent( unsigned int streamIndex, AVCodecContext *codecCtx,
                                    AVCodec *codec, AVStream *stream )
{
   if( avcodec_open2( codecCtx, codec, nullptr ) < 0 )
      return -1;
   m_videoStream.init( streamIndex, stream, codecCtx );
   m_frameOut.create( m_videoStream.codecContext->width, m_videoStream.codecContext->height );
   m_forceCreate = true;
   return 0;
}

double VideoState::getCurrentPts()
{
   return m_pictQueue.queue[m_pictQueue.readIndex].pts;
}

int VideoState::getPictQueueSize()
{
   return m_pictQueue.size;
}

VideoState::~VideoState()
{
   sws_freeContext( m_swsContext );
}

AVCodecContext *VideoState::getContext()
{
   return m_videoStream.codecContext;
}
