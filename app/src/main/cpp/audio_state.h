//
// Created by zhernova on 11.03.2020.
//

#pragma once

#include "clock.h"
#include "stream_info.h"
#include "sync_info.h"
#include "audio_packet.h"
#include "audio_buffer.h"
#include "packet_queue.h"

#define AV_NOSYNC_THRESHOLD            10.0
#define AUDIO_DIFF_AVG_NB              20
#define SAMPLE_CORRECTION_PERCENT_MAX  1

class AudioState
{
   AVFrame *m_audioFrame;
   SyncInfo m_syncInfo;

public:

   StreamInfo m_audioStream;
   AudioBuffer m_audioBuffer;
   AudioPacket m_audioPacket;

   AudioState();

   ~AudioState();

   int synchronizeAudio( int samplesSize, Clock clock );

   int audioDecode( int bufferSize, uint8_t *audioBuffer );

   int getSyncType();

   int openAudioComponent( unsigned int streamIndex, AVCodecContext *codecCtx, AVCodec *codec,
                           AVStream *stream );

   int audioDecodeFrame( int *state, PacketQueue *audioq, Clock *clock, AVPacket *flushPkt );

};
