//
// Created by zhernova on 10.03.2020.
//

#include "seeker.h"

int64_t Seeker::streamSeek( int percentToSeek, int64_t duration )
{
   {
      std::unique_lock<std::mutex> lock( m_mutex );
      if( !m_seekReq )
      {
         m_position = static_cast<int64_t>(( (double)duration / 100 ) * percentToSeek);
         m_flags = AVSEEK_FLAG_BACKWARD;
         if( m_position < 0 )
            m_position = 0;
         if( m_position > duration * AV_TIME_BASE )
            m_position = duration * AV_TIME_BASE;
         m_seekReq = true;
      }
   }
   return m_position;
}

bool Seeker::seekFile( AVFormatContext *pFormatCtx )
{
   bool ret = false;
   {
      std::unique_lock<std::mutex> lock( m_mutex );
      if( m_seekReq )
      {
         int err = avformat_seek_file( pFormatCtx, -1, INT64_MIN, m_position, INT64_MAX, m_flags );
         m_seekReq = false;
         if( err >= 0 )
            ret = true;
      }
   }
   return ret;
}
