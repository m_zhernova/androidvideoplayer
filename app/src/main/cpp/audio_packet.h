//
// Created by zhernova on 11.03.2020.
//

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
}

struct AudioPacket
{
   int size;
   AVPacket *packet;
   uint8_t *data;

   ~AudioPacket()
   {
      av_packet_free( &packet );
      delete[] data;
   }
};