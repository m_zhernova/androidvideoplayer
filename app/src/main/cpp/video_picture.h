//
// Created by zhernova on 11.03.2020.
//

#pragma once

extern "C"
{
#include <libavutil/frame.h>
}

struct VideoPicture
{
   AVFrame *picture;
   uint8_t *buffer;
   int width, height;
   int allocated;
   double pts;

   void allocBuffer( size_t size )
   {
      buffer = static_cast<uint8_t *>(av_malloc( size ));
   }

   void freeBuffer()
   {
      av_free( buffer );
      buffer = nullptr;
   }

   VideoPicture()
   {
      picture = av_frame_alloc();
   }

   ~VideoPicture()
   {
      av_frame_free( &picture );
   }
};