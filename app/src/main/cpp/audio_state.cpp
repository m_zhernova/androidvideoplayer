//
// Created by zhernova on 11.03.2020.
//

#include <thread>
#include "audio_state.h"

int AudioState::synchronizeAudio( int samplesSize, Clock clock )
{
   double refClock;
   if( m_syncInfo.avSyncType != e_AV_SYNC_AUDIO_MASTER )
   {
      refClock = clock.getMasterClock( m_audioStream.codecContext,
                                       m_syncInfo.avSyncType,
                                       m_audioBuffer.getCurrentBufferSize() );
      double diff = clock.getAudioClock( m_audioStream.codecContext,
                                         m_audioBuffer.getCurrentBufferSize() ) - refClock;
      if( diff < AV_NOSYNC_THRESHOLD )
      {
         m_syncInfo.audioDiffCum = diff + m_syncInfo.audioDiffAvgCoef * m_syncInfo.audioDiffCum;
         if( m_syncInfo.audioDiffAvgCount < AUDIO_DIFF_AVG_NB )
            m_syncInfo.audioDiffAvgCount++;
         else
         {
            double avgDiff = m_syncInfo.audioDiffCum * ( 1.0 - m_syncInfo.audioDiffAvgCoef );
            if( fabs( avgDiff ) >= m_syncInfo.audioDiffThreshold )
            {
               int wantedSize = samplesSize + static_cast<int>(diff *
                                                               m_audioStream.codecContext
                                                                            ->sample_rate *
                                                               2 *
                                                               m_audioStream.codecContext
                                                                            ->channels);
               int minSize = samplesSize * ( ( 100 - SAMPLE_CORRECTION_PERCENT_MAX ) / 100 );
               int maxSize = samplesSize * ( ( 100 + SAMPLE_CORRECTION_PERCENT_MAX ) / 100 );
               if( wantedSize < minSize )
                  wantedSize = minSize;
               else if( wantedSize > maxSize )
                  wantedSize = maxSize;
               if( wantedSize < samplesSize )
                  samplesSize = wantedSize;
               else if( wantedSize > samplesSize )
               {
                  uint8_t *samplesEnd;
                  uint8_t *q;
                  int nb;
                  nb = ( samplesSize - wantedSize );
                  samplesEnd =
                        (uint8_t *)reinterpret_cast<const short &>(m_audioBuffer.buffer) +
                        samplesSize - ( 2 * m_audioStream.codecContext->channels );
                  q = samplesEnd + ( 2 * m_audioStream.codecContext->channels );
                  while( nb > 0 )
                  {
                     memcpy( q,
                             samplesEnd,
                             static_cast<size_t>(2 * m_audioStream.codecContext->channels) );
                     q += 2 * m_audioStream.codecContext->channels;
                     nb -= 2 * m_audioStream.codecContext->channels;
                  }
                  samplesSize = wantedSize;
               }
            }
         }
      }
      else
      {
         m_syncInfo.audioDiffAvgCount = 0;
         m_syncInfo.audioDiffCum = 0;
      }
   }
   m_audioBuffer.size = static_cast<unsigned int>(samplesSize);
   return samplesSize;
}

int AudioState::audioDecode( int bufferSize, uint8_t *audioBuffer )
{
   bool gotFrame = false;
   int ret = m_audioStream.decode( m_audioFrame, &gotFrame, m_audioPacket.packet );
   if( ret < 0 )
   {
      m_audioPacket.size = 0;
      return -1;
   }
   int dataSize = 0;
   if( gotFrame )
   {
      uint8_t **dstData = resample( dataSize,
                                    m_audioFrame->nb_samples,
                                    m_audioFrame->sample_rate,
                                    m_audioFrame->channel_layout,
                                    m_audioStream.getFormat(),
                                    AVSampleFormat::AV_SAMPLE_FMT_S16,
                                    m_audioFrame->channels,
                                    m_audioFrame->extended_data );
      if( dataSize < 0 )
      {
         av_packet_free( &m_audioPacket.packet );
      }
      assert( dataSize <= bufferSize );
      memcpy( audioBuffer, dstData[0], static_cast<size_t>(dataSize) );
      av_freep( &dstData[0] );
      av_freep( &dstData );
   }
   m_audioPacket.data += m_audioFrame->pkt_size;
   m_audioPacket.size -= m_audioFrame->pkt_size;
   return dataSize;
}

int AudioState::getSyncType()
{
   return m_syncInfo.avSyncType;
}

AudioState::~AudioState()
{
   av_frame_free( &m_audioFrame );
}

AudioState::AudioState()
{
   m_audioFrame = av_frame_alloc();
}

int AudioState::openAudioComponent( unsigned int streamIndex, AVCodecContext *codecCtx,
                                    AVCodec *codec, AVStream *stream )
{
   if( avcodec_open2( codecCtx, codec, nullptr ) < 0 )
      return -1;
   m_audioStream.init( streamIndex, stream, codecCtx );
   m_audioBuffer.size = 0;
   m_audioBuffer.index = 0;
   memset( &m_audioPacket.packet, 0, sizeof( m_audioPacket.packet ) );
   return 0;
}

int
AudioState::audioDecodeFrame( int *state, PacketQueue *audioq, Clock *clock, AVPacket *flushPkt )
{
   m_audioPacket.packet = av_packet_alloc();
   if( *state == e_OPEN_OTHER || *state == e_QUIT )
   {
      av_packet_free( &m_audioPacket.packet );
      return -1;
   }
   while( *state == e_PLAYING )
   {
      while( m_audioPacket.size > 0 )
      {
         int dataSize = audioDecode( sizeof( m_audioBuffer.buffer ), m_audioBuffer.buffer );
         if( dataSize == -1 )
            break;
         if( dataSize <= 0 )
            continue;
         clock->audioClock += dataSize / ( 2 *
                                           static_cast<double>(m_audioStream.codecContext
                                                                            ->channels) *
                                           m_audioStream.codecContext->sample_rate );
         av_packet_free( &m_audioPacket.packet );
         return dataSize;
      }
      av_packet_unref( m_audioPacket.packet );
      if( audioq->get( 1, *m_audioPacket.packet, state ) < 0 )
      {
         av_free( m_audioPacket.packet );
         return -1;
      }
      if( m_audioPacket.packet->data == flushPkt->data )
         m_audioStream.flushBuffers();
      m_audioPacket.data = m_audioPacket.packet->data;
      m_audioPacket.size = m_audioPacket.packet->size;
      if( m_audioPacket.packet->pts != AV_NOPTS_VALUE )
         clock->audioClock = av_q2d( m_audioStream.getTimeBase() ) *
                             m_audioPacket.packet->pts;
   }
   av_packet_free( &m_audioPacket.packet );
   return 0;
}
