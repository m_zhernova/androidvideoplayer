//
// Created by zhernova on 11.03.2020.
//
#pragma once

extern "C"
{
#include <libavutil/imgutils.h>
}

#include <mutex>

struct FrameOut
{
   std::mutex mutex;
   std::condition_variable isWritten;
   std::unique_ptr<uint8_t[]> frame;
   std::pair<size_t, size_t> params;

public:
   void create( int width, int height )
   {
      params.first = static_cast<size_t>( width );
      params.second = static_cast<size_t>( height);
      size_t bufferSize = static_cast<size_t>(av_image_get_buffer_size( AV_PIX_FMT_RGBA,
                                                                        params.first,
                                                                        params.second,
                                                                        1 ));
      frame = std::make_unique<uint8_t[]>( bufferSize );
   }
};
