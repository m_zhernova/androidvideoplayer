//
// Created by zhernova on 08.01.2020.
//
#include "video_lib.h"
#include "helpers.h"
#include <cstdio>

extern "C"
{
#include <libavcodec/jni.h>
}

#include <android/log.h>
#include <unistd.h>
#include <thread>


int VideoPlayer::openStreamComponent( unsigned int streamIndex )
{
   JNIEnv *env;
   m_javaVm->AttachCurrentThread( &env, nullptr );
   if( streamIndex < 0 || streamIndex >= m_formatCtx->nb_streams )
   {
      m_javaVm->DetachCurrentThread();
      return -1;
   }
   AVCodec *codec = nullptr;
   if( m_formatCtx->streams[streamIndex]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO )
   {
      av_jni_set_java_vm( m_javaVm, nullptr );
      const std::string codecName = "h264_mediacodec";
      codec = avcodec_find_decoder_by_name( codecName.c_str() );
   }
   else
      codec = avcodec_find_decoder( m_formatCtx->streams[streamIndex]->codecpar->codec_id );
   if( !codec )
   {
      m_javaVm->DetachCurrentThread();
      return -1;
   }
   AVCodecContext *codecCtx = avcodec_alloc_context3( codec );
   if( !codecCtx )
      avformat_close_input( &m_formatCtx );
   int result =
         avcodec_parameters_to_context( codecCtx, m_formatCtx->streams[streamIndex]->codecpar );
   if( result < 0 )
   {
      avformat_close_input( &m_formatCtx );
      avcodec_free_context( &codecCtx );
   }
   switch( codecCtx->codec_type )
   {
      case AVMEDIA_TYPE_AUDIO:
      {
         initAudioDevice( env, codecCtx );
         if( m_audioState.openAudioComponent( streamIndex, codecCtx, codec,
                                              m_formatCtx->streams[streamIndex] ) < 0 )
         {
            avcodec_free_context( &codecCtx );
            m_javaVm->DetachCurrentThread();
            return -1;
         }
         m_audioThread = std::thread( &VideoPlayer::audioCallback, this );
         break;
      }
      case AVMEDIA_TYPE_VIDEO:
      {
         if( m_videoState.openVideoComponent( streamIndex, codecCtx, codec,
                                              m_formatCtx->streams[streamIndex] ) < 0 )
         {
            avcodec_free_context( &codecCtx );
            m_javaVm->DetachCurrentThread();
            return -1;
         }
         m_frameTimerInfo.frameTimer = av_gettime() / 1000000.0;
         m_frameTimerInfo.frameLastDelay = 40e-3;
         m_clock.videoCurrentPtsTime = av_gettime();
         double frameRate = (double)m_videoState.m_videoStream.stream->avg_frame_rate.num /
                            (double)m_videoState.m_videoStream.stream->avg_frame_rate.den;
         m_duration = static_cast<int64_t>(( m_formatCtx->duration / AV_TIME_BASE ) * frameRate);
         m_currentVideoFrame = 0;
         m_videoThread = std::thread( &VideoPlayer::videoThread, this );
         break;
      }
      default:
         break;
   }
   m_javaVm->DetachCurrentThread();
   return 0;
}

void VideoPlayer::videoThread()
{
   AVPacket *packet = av_packet_alloc();
   av_init_packet( packet );
   AVFrame *frame = av_frame_alloc();
   char buffer[100];
   sprintf( buffer, "+++++++++m_videoThread started+++++++++++++++++\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
   for( ;; )
   {
      if( m_state == e_OPEN_OTHER || m_state == e_QUIT )
         break;
      av_packet_unref( packet );
      if( m_videoq.get( 1, *packet, &m_state ) < 0 )
         break;
      if( packet->data == m_flushPkt->data )
         avcodec_flush_buffers( m_videoState.getContext() );
      bool frameFinished = false;
      m_videoState.m_videoStream.decode( frame, &frameFinished, packet );
      double pts;
      if( packet->dts != AV_NOPTS_VALUE )
         pts = (int64_t)frame->best_effort_timestamp;
      else
         pts = 0;
      pts *= av_q2d( m_videoState.m_videoStream.stream->time_base );
      if( frameFinished )
      {
         int ret = 0;
         do
         {
            pts = m_clock.synchronizeVideo( m_videoState.getContext()->time_base,
                                            *frame,
                                            pts );
         } while( m_state == e_PAUSED );
         ret = m_videoState.queuePicture( pts,
                                          frame,
                                          m_videoState.getContext(),
                                          &m_state );
         if( ret < 0 )
            break;
         m_currentVideoFrame++;
      }
   }
   av_frame_free( &frame );
   av_packet_free( &packet );
   sprintf( buffer, "m_videoThread finished \n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
}

int VideoPlayer::initDecoding()
{
   AVFormatContext *formatContext = avformat_alloc_context();
   int err = avformat_open_input( &formatContext, m_fileName.c_str(), nullptr, nullptr );
   if( err != 0 )
      return -1;
   m_formatCtx = formatContext;
   err = avformat_find_stream_info( m_formatCtx, nullptr );
   if( err < 0 )
      return -2;
   av_dump_format( m_formatCtx, 0, m_fileName.c_str(), 0 );
   int videoIndex = -1;
   int audioIndex = -1;
   for( unsigned int i = 0; i < m_formatCtx->nb_streams; i++ )
   {
      if( m_formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO && videoIndex < 0 )
         videoIndex = i;
      if( m_formatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO && audioIndex < 0 )
         audioIndex = i;
   }
   audioIndex >= 0 ? openStreamComponent( static_cast<unsigned int>(audioIndex) ) : audioIndex = 0;
   videoIndex >= 0 ? openStreamComponent( static_cast<unsigned int>(videoIndex) ) : videoIndex = 0;
   if( m_videoState.m_videoStream.streamInd < 0 || m_audioState.m_audioStream.streamInd < 0 )
   {
      joinAllThreads( 1 );
      return -3;
   }
   return 0;
}

void VideoPlayer::flushQueues()
{
   if( m_audioState.m_audioStream.streamInd >= 0 )
   {
      m_audioq.flush();
      m_audioq.put( *m_flushPkt, *m_flushPkt );
   }
   if( m_videoState.m_videoStream.streamInd >= 0 )
   {
      m_videoq.flush();
      m_videoq.put( *m_flushPkt, *m_flushPkt );
   }
}

int VideoPlayer::decodeThread()
{
   char buffer[100];
   sprintf( buffer, "************************decodeThread started****************************\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
   int err = initDecoding();
   if( err < 0 )
   {
      sprintf( buffer, "*****decodeThread finished initDecoding returned %i****\n", err );
      __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
      return -1;
   }
   int packetSize = 0;
   AVPacket avPacket;
   for( ;; )
   {
      if( m_state == e_OPEN_OTHER || m_state == e_QUIT )
      {
         sprintf( buffer, "decodeThread finished state %i \n", m_state );
         __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
         break;
      }
      if( m_state == e_PLAYING )
      {
         AVPacket *packet = &avPacket;
         av_init_packet( packet );
         if( m_seeker.seekFile( m_formatCtx ) )
            flushQueues();
         err = av_read_frame( m_formatCtx, packet );
         if( err == AVERROR_EOF )
         {
            av_packet_unref( packet );
            packetSize = 0;
         }
         else
         {
            if( packetSize == 0 && err >= 0 )
               packetSize = packet->size;
            int maxQueue = 5 * 256 * m_bufferSize;
            if( m_videoq.getSize() >= maxQueue || m_audioq.getSize() >= maxQueue )
            {
               usleep( 100000 );
               continue;
            }
            err = -1;
            if( packet->stream_index == m_videoState.m_videoStream.streamInd )
               err = m_videoq.put( *packet, *m_flushPkt );
            else if( packet->stream_index == m_audioState.m_audioStream.streamInd )
               err = m_audioq.put( *packet, *m_flushPkt );
            if( err < 0 )
            {
               av_packet_unref( packet );
               packetSize = 0;
            }
         }
      }
   }
   while( m_state == e_PLAYING || m_state == e_PAUSED )
   {
      sprintf( buffer, "decodeThread sleep\n" );
      __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
      usleep( 100000 );
   }
   if( m_state == e_QUIT )
      joinAllThreads( true );
   sprintf( buffer, "decodeThread finish normal\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
   return 0;
}

int VideoPlayer::refreshTimer()
{
   if( m_videoState.m_videoStream.stream )
   {
      if( e_PAUSED == m_state )
      {
         m_frameTimerInfo.frameTimer += ( av_gettime() / 1000000.0 ) - m_frameTimerInfo.frameTimer;
      }
      if( m_videoState.getPictQueueSize() <= 0 )
         scheduleRefresh( 1 );
      else
      {
         m_clock.videoCurrentPts = m_videoState.getCurrentPts();
         m_clock.videoCurrentPtsTime = av_gettime();
         double delay = m_clock.videoCurrentPts - m_frameTimerInfo.frameLastPts;
         if( delay <= 0 || delay >= 1.0 )
            delay = m_frameTimerInfo.frameLastDelay;
         m_frameTimerInfo.frameLastDelay = delay;
         m_frameTimerInfo.frameLastPts = m_clock.videoCurrentPts;
         if( m_audioState.getSyncType() != e_AV_SYNC_VIDEO_MASTER )
         {
            double reClock = m_clock.getMasterClock( m_audioState.m_audioStream.codecContext,
                                                     m_audioState.getSyncType(),
                                                     m_audioState.m_audioBuffer
                                                                 .getCurrentBufferSize() );
            double diff = m_clock.videoCurrentPts - reClock;
            double syncThreshold = ( delay > AV_SYNC_THRESHOLD ) ? delay : AV_SYNC_THRESHOLD;
            if( fabs( diff ) < AV_NOSYNC_THRESHOLD )
            {
               if( diff <= -syncThreshold )
                  delay = 0;
               else if( diff >= syncThreshold )
                  delay = 2 * delay;
            }
         }
         m_frameTimerInfo.frameTimer += delay;
         double actualDelay = m_frameTimerInfo.frameTimer - ( av_gettime() / 1000000.0 );
         scheduleRefresh( static_cast<uint32_t>(actualDelay * 1000) );
         m_videoState.fillFrame();
         m_videoState.m_frameOut.isWritten.notify_all();
         m_changeFrame = true;
         return 0;
      }
   }
   else
      scheduleRefresh( 100 );
   return -1;
}

void VideoPlayer::audioCallback()
{
   char buffer[100];
   sprintf( buffer, "--------------------audioCallback started----------------\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
   JNIEnv *env = nullptr;
   m_javaVm->AttachCurrentThread( &env, nullptr );
   jbyteArray data = env->NewByteArray( static_cast<jsize>( m_bufferSize ) );
   jclass audioTrack = env->FindClass( "android/media/AudioTrack" );
   jmethodID write = env->GetMethodID( audioTrack, "write", "([BII)I" );
   while( m_state == e_PLAYING || m_state == e_PAUSED )
   {
      if( m_audioState.m_audioBuffer.index >= m_audioState.m_audioBuffer.size )
      {
         int audioSize = m_audioState.audioDecodeFrame( &m_state, &m_audioq, &m_clock,
                                                        m_flushPkt );
         if( audioSize < 0 )
         {
            m_audioState.m_audioBuffer.size = static_cast<unsigned int>(m_bufferSize);
            memset( m_audioState.m_audioBuffer.buffer, 0, m_audioState.m_audioBuffer.size );
         }
         else
            m_audioState.synchronizeAudio( audioSize, m_clock );
         m_audioState.m_audioBuffer.index = 0;
         int toWriteSize = m_audioState.m_audioBuffer.size - m_audioState.m_audioBuffer.index;
         env->SetByteArrayRegion( data,
                                  0,
                                  toWriteSize,
                                  (jbyte *)m_audioState.m_audioBuffer.buffer +
                                  m_audioState.m_audioBuffer.index );
         env->CallIntMethod( m_audioDevice, write, data, 0, (jint)toWriteSize );
         m_audioState.m_audioBuffer.index += toWriteSize;
      }
   }
   sprintf( buffer, "=================audioCallback finished==================\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
   env->DeleteGlobalRef( m_audioDevice );
   m_javaVm->DetachCurrentThread();
}

void VideoPlayer::joinAllThreads( bool quit )
{
   if( quit )
      m_state = e_QUIT;
   m_audioq.notify();
   m_videoq.notify();
   m_videoState.notify();
   m_videoState.m_frameOut.isWritten.notify_all();
   if( m_videoThread.joinable() )
      m_videoThread.join();
   if( m_parseThread.joinable() )
      m_parseThread.join();
   if( m_displayThread.joinable() )
      m_displayThread.join();
   if( m_audioThread.joinable() )
      m_audioThread.join();
}

VideoPlayer &VideoPlayer::instance()
{
   static VideoPlayer inst;
   return inst;
}

bool VideoPlayer::pauseOrContinue()
{
   if( m_state == e_PLAYING )
   {
      m_state = e_PAUSED;
      return true;
   }
   else if( m_state == e_PAUSED )
   {
      m_state = e_PLAYING;
      return false;
   }
   return false;
}

void VideoPlayer::initAudioDevice( JNIEnv *env, AVCodecContext *codecCtx )
{
   const char *audioTrackClass = "android/media/AudioTrack";
   const char *audioFormatClass = "android/media/AudioFormat";
   const char *audioManagerClass = "android/media/AudioManager";
   jclass audioTrack = env->FindClass( audioTrackClass );
   jclass audioFormat = env->FindClass( audioFormatClass );
   jclass audioManager = env->FindClass( audioManagerClass );
   jmethodID play = env->GetMethodID( audioTrack, "play", "()V" );
   jmethodID constructor = env->GetMethodID( audioTrack, "<init>", "(IIIIII)V" );
   jfieldID streamTypeId = env->GetStaticFieldID( audioManager, "STREAM_MUSIC", "I" );
   jfieldID formatId = env->GetStaticFieldID( audioFormat, "ENCODING_PCM_16BIT", "I" );
   jfieldID modeId = env->GetStaticFieldID( audioTrack, "MODE_STREAM", "I" );
   jint sampleRate = codecCtx->sample_rate;
   jint channelsCount = codecCtx->channels;
   jint channels = getChannelsMask( audioFormat, channelsCount, env );
   jmethodID getBufferSize = env->GetStaticMethodID( audioTrack, "getMinBufferSize", "(III)I" );
   jint jbufferSize =
         env->CallStaticIntMethod( audioTrack, getBufferSize, sampleRate, channels, 2 );
   m_bufferSize = static_cast<size_t>( jbufferSize );
   jint streamType = env->GetStaticIntField( audioManager, streamTypeId );
   jint format = env->GetStaticIntField( audioFormat, formatId );
   jint mode = env->GetStaticIntField( audioTrack, modeId );
   auto obj = env->NewObject( audioTrack,
                              constructor,
                              streamType,
                              sampleRate,
                              channels,
                              format,
                              m_bufferSize,
                              mode );
   m_audioDevice = env->NewGlobalRef( obj );
   env->DeleteLocalRef( obj );
   env->CallVoidMethod( m_audioDevice, play );
}

void VideoPlayer::setParams( jobject seekBar, double width, double height, JNIEnv *env )
{
   if( nullptr != env )
   {
      m_javaVm = nullptr;
      env->GetJavaVM( &m_javaVm );
   }
   m_screenParams.first = static_cast<int>(width);
   m_screenParams.second = static_cast<int>(height);
}

void VideoPlayer::displayThread()
{
   char buffer[100];
   sprintf( buffer, "--------------------displayThread started----------------\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
   while( m_state == e_PLAYING || m_state == e_PAUSED )
   {
      usleep( static_cast<unsigned int>(m_sleepTime) * 100 );
      VideoPlayer::refreshTimer();
   }
   sprintf( buffer, "--------------displayThread finished----------------\n" );
   __android_log_write( ANDROID_LOG_ERROR, "Warning", buffer );
}

void VideoPlayer::scheduleRefresh( uint32_t delay )
{
   m_sleepTime = delay;
   if( !m_displayThread.joinable() )
      m_displayThread = std::thread( &VideoPlayer::displayThread, this );
}

VideoPlayer::VideoPlayer()
      : m_state( e_UNINITILIZED )
        , m_duration( 0 )
        , m_changeFrame( false )
        , m_audioDevice( nullptr )
        , m_bufferSize( 0 )
        , m_sleepTime( 0 )
        , m_currentVideoFrame( 0 )
        , m_screenParams( 0, 0 )
        , m_javaVm( nullptr )
        , m_flushPkt( av_packet_alloc() )
{
   av_register_all();
   av_init_packet( m_flushPkt );
   m_flushPkt->data = (uint8_t *)"FLUSH";
   m_formatCtx = avformat_alloc_context();
}

VideoPlayer::~VideoPlayer()
{
   JNIEnv *env;
   m_javaVm->AttachCurrentThread( &env, nullptr );
   m_javaVm->DetachCurrentThread();
   av_packet_free( &m_flushPkt );
   avcodec_free_context( &m_videoState.m_videoStream.codecContext );
   avcodec_free_context( &m_audioState.m_audioStream.codecContext );
   avformat_free_context( m_formatCtx );
}

bool VideoPlayer::getFlag()
{
   if( !m_changeFrame || m_state != e_PLAYING )
      return false;
   else if( m_changeFrame )
   {
      m_changeFrame = false;
      return true;
   }
}

bool VideoPlayer::openFile( const std::string &name )
{
   bool wasPaused = true;
   if( m_state == e_PLAYING )
   {
      m_state = e_PAUSED;
      wasPaused = false;
   }
   if( name != m_fileName )
   {
      m_state = e_OPEN_OTHER;
      joinAllThreads( false );
      m_videoq.flush();
      m_audioq.flush();
      m_fileName = name;
      m_videoState.m_videoStream.streamInd = -1;
      m_audioState.m_audioStream.streamInd = -1;
      m_parseThread = std::thread( &VideoPlayer::decodeThread, this );
      scheduleRefresh( 40 );
      m_state = e_PLAYING;
   }
   else
   {
      if( m_state == e_PLAYING && wasPaused )
         m_state = e_PAUSED;
      else if( !wasPaused )
         m_state = e_PLAYING;
      return false;
   }
   return true;
}

FrameOut *VideoPlayer::getFrameOut()
{
   return &m_videoState.m_frameOut;
}

std::pair<int, int> VideoPlayer::getScreenParams()
{
   return m_screenParams;
}

void VideoPlayer::seekPercent( int percentToSeek )
{
   int64_t pos = m_seeker.streamSeek( percentToSeek, m_formatCtx->duration );
   double frameRate = m_videoState.m_videoStream.getFrameRate();
   m_currentVideoFrame = static_cast<int64_t>(( (double)pos / AV_TIME_BASE ) * frameRate );
}

int VideoPlayer::getPercent()
{
   double percent = 0;
   if( m_duration != 0 )
   {
      percent = (double)m_currentVideoFrame * 100 / m_duration;
   }
   return static_cast<int>(percent);
}

int VideoPlayer::getState()
{
   return m_state;
}
