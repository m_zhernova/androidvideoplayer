//
// Created by zhernova on 11.03.2020.
//

#pragma once

extern "C"
{
#include <libavcodec/avcodec.h>
}

#define MAX_AUDIO_FRAME_SIZE           192000

struct AudioBuffer
{
   uint8_t buffer[( MAX_AUDIO_FRAME_SIZE * 3 ) / 2];
   unsigned int size;
   unsigned int index;

   int getCurrentBufferSize()
   {
      return size - index;
   }
};