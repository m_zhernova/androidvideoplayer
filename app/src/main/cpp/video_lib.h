//
// Created by zhernova on 08.01.2020.
//

#pragma  once

#include <thread>
#include "video_state.h"
#include "packet_queue.h"
#include "seeker.h"
#include "clock.h"
#include "audio_state.h"

#define AV_SYNC_THRESHOLD              0.01

struct SwsContext;

struct FrameTimerInfo
{
   double frameTimer;
   double frameLastPts;
   double frameLastDelay;
};

class VideoPlayer
{
private:
   jobject m_audioDevice;
   int m_bufferSize;
   std::pair<int, int> m_screenParams;
   JavaVM *m_javaVm;
   AVPacket *m_flushPkt;
   int m_state;
   int64_t m_currentVideoFrame;
   std::atomic_bool m_changeFrame;
   uint32_t m_sleepTime;
   int64_t m_duration;
   Seeker m_seeker;
   PacketQueue m_audioq;
   PacketQueue m_videoq;
   std::thread m_parseThread;
   std::thread m_videoThread;
   std::thread m_audioThread;
   std::thread m_displayThread;
   AudioState m_audioState;
   VideoState m_videoState;
   std::string m_fileName;
   Clock m_clock;
   AVFormatContext *m_formatCtx;
   FrameTimerInfo m_frameTimerInfo;

   void scheduleRefresh( uint32_t delay );

   void displayThread();

   int openStreamComponent( unsigned int streamIndex );

   void videoThread();

   int decodeThread();

   void audioCallback();

   void initAudioDevice( JNIEnv *env, AVCodecContext *codecCtx );

   int initDecoding();

   void flushQueues();

   VideoPlayer();

public:

   static VideoPlayer &instance();

   void seekPercent( int percentToSeek );

   bool pauseOrContinue();

   bool openFile( const std::string &fd );

   void setParams( jobject seekBar, double width, double height, JNIEnv *env );

   int refreshTimer();

   std::pair<int, int> getScreenParams();

   void joinAllThreads( bool quit );

   bool getFlag();

   ~VideoPlayer();

   FrameOut *getFrameOut();

   int getPercent();

   int getState();
};
