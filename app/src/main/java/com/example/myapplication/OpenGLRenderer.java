package com.example.myapplication;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

class OpenGLRenderer implements GLSurfaceView.Renderer {

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        onCreated();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        onChanged(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        onDraw();
    }

    static {
        System.loadLibrary("videoLib");
    }

    native public void onCreated();

    native public void onChanged(int width, int height);

    native public void onDraw();
}